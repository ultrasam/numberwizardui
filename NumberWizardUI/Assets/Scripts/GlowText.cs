﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GlowText : MonoBehaviour {

    public float glowSpeed = 1f;
    private TextMeshProUGUI myTextMeshPro;
	// Use this for initialization
	void Start () {
        myTextMeshPro = GetComponent<TextMeshProUGUI>();
        myTextMeshPro.fontSharedMaterial.EnableKeyword("GLOW_ON");
        myTextMeshPro.fontSharedMaterial.SetFloat(ShaderUtilities.ID_GlowPower, 0f);
        myTextMeshPro.fontSharedMaterial.SetFloat(ShaderUtilities.ID_GlowOffset, 1f);
        myTextMeshPro.fontSharedMaterial.SetFloat(ShaderUtilities.ID_GlowOuter, 1f);
        myTextMeshPro.fontSharedMaterial.SetFloat("_GlowInner", 0.4f);
        StartCoroutine(MakeItGlow(glowSpeed));
    }
	
	// Update is called once per frame
	void Update () {


    }

    IEnumerator MakeItGlow(float speedSeconds)
    {
        bool increasing = true;
        float valueToAdd = Time.deltaTime * speedSeconds;
        float currentValue = 0f;
        while (true)
        {
            if(increasing)
            {
                currentValue += valueToAdd;
            }
            else
            {
                currentValue -= valueToAdd;
            }
            if(currentValue >= 1f)
            {
                increasing = false;
                currentValue = 1f;
            } else if (currentValue <= 0f)
            {
                increasing = true;
                currentValue = 0f;
            }
            myTextMeshPro.fontSharedMaterial.SetFloat(ShaderUtilities.ID_GlowPower, currentValue);
            yield return null;
        }
    }
}