﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberWizard : MonoBehaviour {
    int guess;
    [SerializeField] int lowestNumber = 1;
    [SerializeField] int highestNumber = 1000;
    [SerializeField] TextMeshProUGUI headingText;
    [SerializeField] TextMeshProUGUI guessText;

    // Use this for initialization
    void Start () {
        StartGame();
    }

    // Update is called once per frame
    void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            OnPressHigher();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            OnPressLower();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            StartGame();
        }
    }

    void StartGame()
    {
        NextGuess();
        headingText.text = "I will read your mind.\nPick a Number between " + lowestNumber + " and " + highestNumber +".";
    }

    void NextGuess()
    {
        guess = Random.Range(lowestNumber, highestNumber + 1);
        if(guess > highestNumber)
        {
            guess = highestNumber;
        }
        guessText.text = guess.ToString();
    }

    public void OnPressHigher()
    {
        if (lowestNumber < highestNumber)
        {
            lowestNumber = guess + 1;
            NextGuess();
        }
    }

    public void OnPressLower()
    {
        if (highestNumber > lowestNumber)
        {
            highestNumber = guess - 1;
            NextGuess();
        }
    }
}
